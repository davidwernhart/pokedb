USE Pokemon
GO
--sets a new arena head after a fight, if necessary
CREATE OR ALTER PROCEDURE sp_setNewHead
@winnerid int, @loserid int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	BEGIN TRANSACTION

	IF((SELECT count(*) FROM trainer WHERE train_id = @winnerid) = 0) OR ((SELECT count(*) FROM trainer WHERE train_id = @loserid) = 0)
		BEGIN
			RAISERROR('Trainer existiert nicht', 16, 1);
			ROLLBACK TRANSACTION
			RETURN
		END
	ELSE
		BEGIN
		-- 0 = non, 1 = oldtrainer, 2 = newtrainer, 3 = both  are heads
			DECLARE @checkHead int
			EXEC @checkHead = sp_checkTrainers @winnerid, @loserid
			if @checkHead = 0
				BEGIN
					ROLLBACK TRANSACTION
					RETURN
				END
			else if @checkHead = 3
				BEGIN
					ROLLBACK TRANSACTION
					RETURN
				END
			else if @checkHead = 1
				BEGIN 
					ROLLBACK TRANSACTION
					RETURN
				END
				-- if loser was arena head, winner becomes the new head
			else if @checkHead = 2
				BEGIN
					DECLARE @arenaid int
					DECLARE @arenaName VARCHAR(50)
					DECLARE @winnername VARCHAR(50)
					--get arena info
					SELECT @arenaid = arena_id, @arenaName = name FROM arenen
					WHERE fk_leiter = @loserid
					--change head
					UPDATE arenen SET fk_leiter = @winnerid
					WHERE arena_id = @arenaid
					--get @winner name
					SELECT @winnername = name FROM trainer
					WHERE train_id = @winnerid

					print @winnername+ ' is now head of '+ @arenaname
				END
			END
		COMMIT TRANSACTION
END

