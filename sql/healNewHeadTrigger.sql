USE Pokemon
GO

CREATE OR ALTER TRIGGER tr_healNewHead ON arenen
FOR UPDATE 
AS 
BEGIN
		DECLARE @newHeadid int
		SELECT @newHeadid = fk_leiter FROM inserted
		EXEC sp_pokecenter @newHeadid
END
