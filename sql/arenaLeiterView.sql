USE Pokemon
GO

CREATE OR ALTER VIEW vw_arenaLeiter
AS
SELECT arenen.name as arena, trainer.name as trainer
FROM arenen
INNER JOIN trainer
ON train_id = fk_leiter
GO 

SELECT * FROM vw_arenaLeiter