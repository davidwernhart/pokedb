USE Pokemon;

GO
CREATE OR ALTER PROCEDURE sp_fight--TODO: Error handling, transaction
@trainerid int, @enemyid int
AS
BEGIN
		IF((SELECT count(*) FROM trainer WHERE train_id = @trainerid) = 0) OR ((SELECT count(*) FROM trainer WHERE train_id = @enemyid) = 0)
			BEGIN
				RAISERROR('Trainer existiert nicht', 16, 1);
			END
		ELSE
			DECLARE @s_yourlife int
			DECLARE @s_mylife int
			DECLARE @trainername VARCHAR(50)
			DECLARE @enemyname VARCHAR(50)
			--get trainer name
			SELECT @trainername = name FROM trainer
			WHERE train_id = @trainerid
			--get enemy name
			SELECT @enemyname = name FROM trainer
			WHERE train_id = @enemyid
			EXEC sp_trainerHealth @trainerid, @s_mylife output
			EXEC sp_trainerHealth @enemyid, @s_yourlife output
			print @trainername+ ': ' +CAST(@s_mylife AS VARCHAR)+' '+@enemyname+': '+CAST(@s_yourlife AS VARCHAR)
			print 'starting fight'
			DECLARE @mycurrentpokid int
			DECLARE @mycurrentlifepoints int

			DECLARE cr_mycursor CURSOR FOR
			SELECT fk_pok_id,lebenspunkte FROM pok_train
			WHERE fk_train_id = @trainerid AND lebenspunkte > 0
			OPEN cr_mycursor
			FETCH NEXT FROM cr_mycursor INTO @mycurrentpokid, @mycurrentlifepoints
			WHILE @@FETCH_STATUS = 0 --iterate through all pokemon of the trainer
			BEGIN

				DECLARE @yourcurrentpokid int
				DECLARE @yourcurrentlifepoints int

				DECLARE cr_yourcursor CURSOR FOR
				SELECT fk_pok_id,lebenspunkte FROM pok_train
				WHERE fk_train_id = @enemyid AND lebenspunkte > 0
				OPEN cr_yourcursor
				FETCH NEXT FROM cr_yourcursor INTO @yourcurrentpokid, @yourcurrentlifepoints
				WHILE @@FETCH_STATUS = 0 --iterate through all pokemon of the enemy
				BEGIN
					DECLARE @myname varchar(50)
					DECLARE @mytype varchar(50)
					SELECT @myname = name, @mytype = typ FROM pokemon
					WHERE @mycurrentpokid = pok_id

					DECLARE @yourname varchar(50)
					DECLARE @yourtype varchar(50)
					SELECT @yourname = name, @yourtype = typ FROM pokemon
					WHERE @yourcurrentpokid = pok_id

					print @myname + ' vs: ' + @yourname

					--get attacks from pokemon

					DECLARE @myattackname varchar(50)
					DECLARE @myattackpoints int
					DECLARE @myattacktype varchar(10)

					DECLARE @yourattackname varchar(50)
					DECLARE @yourattackpoints int
					DECLARE @yourattacktype varchar(10)

					SELECT TOP 1 @myattackname = name, @myattackpoints = angriffspunkte, @myattacktype = typ FROM attacken
					JOIN pok_att ON pok_att.fk_att_id = attacken.att_id
					WHERE pok_att.fk_pok_id = @mycurrentpokid

					SELECT TOP 1 @yourattackname = name, @yourattackpoints = angriffspunkte, @yourattacktype = typ FROM attacken
					JOIN pok_att ON pok_att.fk_att_id = attacken.att_id
					WHERE pok_att.fk_pok_id = @yourcurrentpokid

					--if attack type is the same as pokemon type, it gets multiplied

					if @mytype = @myattacktype
					BEGIN
						 SELECT @myattackpoints = @myattackpoints * 1.5
					END

					if @yourtype = @yourattacktype
					BEGIN
						 SELECT @yourattackpoints = @yourattackpoints * 1.5
					END

					DECLARE @myweakness varchar(10)
					DECLARE @mystrength varchar(10)
					DECLARE @yourweakness varchar(10)
					DECLARE @yourstrength varchar (10)

					SELECT @myweakness = schwach_gegen, @mystrength = effektiv_gegen FROM typen
					WHERE typ = @myattacktype
					SELECT @yourweakness = schwach_gegen, @yourstrength = effektiv_gegen FROM typen
					WHERE typ = @yourattacktype

					--compute type weaknesses

					if @yourtype = @mystrength
					BEGIN
						SELECT @myattackpoints = @myattackpoints * 2
					END
					else if @yourtype = @myweakness
					BEGIN
						SELECT @myattackpoints = @myattackpoints * 0.5
					END

					if @mytype = @yourstrength
					BEGIN
						SELECT @yourattackpoints = @yourattackpoints * 2
					END
					else if @mytype = @yourweakness
					BEGIN
						SELECT @yourattackpoints = @yourattackpoints * 0.5
					END

					print @myname + ' deals ' + CAST(@myattackpoints AS VARCHAR) + ' points damage with: ' + @myattackname
					print @yourname + ' deals ' + CAST(@yourattackpoints AS VARCHAR) + ' points damage with: ' + @yourattackname

					--update life points

					UPDATE pok_train SET lebenspunkte = lebenspunkte - @yourattackpoints
					WHERE fk_train_id = @trainerid AND fk_pok_id = @mycurrentpokid
					UPDATE pok_train SET lebenspunkte = lebenspunkte - @myattackpoints
					WHERE fk_train_id = @enemyid AND fk_pok_id = @yourcurrentpokid
					--fetch next poke if lifepoints of current is <= 0
					DECLARE @myPokeCheck int
					SELECT @myPokeCheck = lebenspunkte FROM pok_train
					WHERE fk_train_id = @trainerid AND fk_pok_id = @mycurrentpokid
					-- break if no poke left in cursor
					if @myPokeCheck <= 0
					BEGIN
						FETCH NEXT FROM cr_mycursor INTO @mycurrentpokid, @mycurrentlifepoints
						if @@FETCH_STATUS != 0
						BEGIN
							BREAK;
						END
					END
					SELECT @myPokeCheck = lebenspunkte FROM pok_train
					WHERE fk_train_id = @enemyid AND fk_pok_id = @yourcurrentpokid

					if @myPokeCheck <= 0
					BEGIN
						FETCH NEXT FROM cr_yourcursor INTO @yourcurrentpokid, @yourcurrentlifepoints
						if @@FETCH_STATUS != 0
						BEGIN
							BREAK;
						END
					END

				END

				CLOSE cr_yourcursor
				DEALLOCATE cr_yourcursor
				FETCH NEXT FROM cr_mycursor INTO @mycurrentpokid, @mycurrentlifepoints
			END
			CLOSE cr_mycursor
			DEALLOCATE cr_mycursor
			--DECLARE @s_yourlife int
			--DECLARE @s_mylife int
			EXEC sp_trainerHealth @trainerid, @s_mylife output
			EXEC sp_trainerHealth @enemyid, @s_yourlife output
			print @trainername+': '+CAST(@s_mylife AS VARCHAR)+' '+@enemyname+': '+CAST(@s_yourlife AS VARCHAR)
			if @s_mylife = 0 AND @s_yourlife > 0
			BEGIN
				print @enemyname+' won'
				EXEC sp_setNewHead @enemyid, @trainerid
			END
			else if @s_mylife > 0 AND @s_yourlife = 0
			BEGIN
				print @trainername+' won'
				EXEC sp_setNewHead @trainerid, @enemyid
			END
			else if @s_mylife = 0 AND @s_yourlife = 0
			BEGIN
				print 'tie'
			END
END
GO

--EXEC sp_fight 1,2;

--EXEC pokecenter 2;