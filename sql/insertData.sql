USE Pokemon
GO


DELETE FROM arenen;
DELETE FROM pok_train;
DELETE FROM trainer;
DELETE FROM pok_att;
DELETE FROM pokemon;
DELETE FROM attacken;
DELETE FROM typen;


--Pokemon erstellen
INSERT INTO pokemon
(name, typ, groesse, gewicht, max_lebenspunkte)
VALUES
('Bisasam', 'Pflanze', 50, 20, 100),
('Bisaknosp', 'Pflanze', 70, 30, 125),
('Bisaflor', 'Pflanze', 100, 50, 150),
('Schiggy', 'Wasser', 70, 30, 100),
('Schillok', 'Wasser', 90, 50, 125),
('Turtok', 'Wasser', 110, 70, 150),
('Glumanda', 'Feuer', 55, 30, 100),
('Glutexo', 'Feuer', 75, 50, 125),
('Glurak', 'Feuer', 95, 80, 150),
('Pikachu', 'Elektro', 35, 15, 100),
('Raichu', 'Elektro', 65, 35, 150);

--Attacken erstellen
INSERT INTO attacken
(name, angriffspunkte, typ)
VALUES
('Tackle', 30, 'Normal'),
('Donnerwelle', 30, 'Elektro'),
('Donnerschock', 60, 'Elektro'),
('Aquaknarre', 40, 'Wasser'),
('Hydropumpe', 70, 'Wasser'),
('Rankenhieb', 30, 'Pflanze'),
('Rasierblatt', 50, 'Pflanze'),
('Glut', 30, 'Feuer'),
('Flammenwerfer', 60, 'Feuer');

--Attacke zuweisen
EXEC sp_learnAttack 'Rankenhieb','Bisasam';
EXEC sp_learnAttack 'Rankenhieb','Bisaknosp';
EXEC sp_learnAttack 'Rasierblatt','Bisaflor';
EXEC sp_learnAttack 'Glut','Glumanda';
EXEC sp_learnAttack 'Glut','Glutexo';
EXEC sp_learnAttack 'Flammenwerfer','Glurak';
EXEC sp_learnAttack 'Aquaknarre','Schiggy';
EXEC sp_learnAttack 'Aquaknarre','Schillok';
EXEC sp_learnAttack 'Hydropumpe','Turtok';
EXEC sp_learnAttack 'Donnerwelle','Pikachu';
EXEC sp_learnAttack 'Donnerschock','Raichu';

--Trainer erstellen
INSERT INTO trainer
(name, age, orden, region)
VALUES
('Alexander', 22, 8,'Kanto'),
('Chino', 22, 8,'Kento'),
('David', 22, 80,'Konto'),
('Liam', 17, 1, 'Favoriten');


EXEC sp_catchPokemon "Alexander","Glumanda";
EXEC sp_catchPokemon "Alexander","Raichu";
EXEC sp_catchPokemon "Liam","Bisasam";
EXEC sp_catchPokemon "Chino","Schillok";
EXEC sp_catchPokemon "Chino","Schiggy";
EXEC sp_catchPokemon "David","Turtok";
EXEC sp_catchPokemon "David","Bisaflor";
EXEC sp_catchPokemon "David","Glurak";

--Typeneffektivität erstellen
INSERT INTO typen VALUES ('Feuer', 'Pflanze', 'Wasser');
INSERT INTO typen VALUES ('Wasser', 'Feuer', 'Pflanze');
INSERT INTO typen VALUES ('Pflanze', 'Wasser', 'Feuer');
INSERT INTO typen VALUES ('Elektro', 'Wasser', 'Wasser');

--Arenen erstellen
INSERT INTO arenen
(name, ort, stufe, fk_leiter)
VALUES
('Steinarena', 'Marmoria-City', 8, 1),
('Wasserarena', 'Azuria-City', 12, 2);

--SELECT * FROM arenen;