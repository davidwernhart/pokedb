USE Pokemon;

GO

CREATE OR ALTER TRIGGER tr_checkLifePoints ON pok_train
FOR UPDATE 
AS 
BEGIN
		DECLARE @life_p int
		DECLARE @t_id int
		DECLARE @p_id int
		SELECT @life_p = lebenspunkte, @t_id = fk_train_id, @p_id = fk_pok_id FROM inserted
		if @life_p <= 0
		BEGIN
			UPDATE pok_train SET lebenspunkte = 0
			WHERE fk_train_id = @t_id AND fk_pok_id = @p_id
			DECLARE @p_name VARCHAR(50)
			SELECT @p_name = name FROM pokemon
			WHERE @p_id = pok_id
			print @p_name +' got defeated'
		END

END
