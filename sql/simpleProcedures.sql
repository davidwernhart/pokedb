--db configuration for app access
USE Pokemon;
GO
EXEC sp_configure 'CONTAINED DATABASE AUTHENTICATION', 1;
GO
RECONFIGURE;
GO
USE master;
ALTER DATABASE [Pokemon] SET CONTAINMENT = PARTIAL;
GO
USE Pokemon
CREATE USER app WITH PASSWORD ='test';
GRANT SELECT, INSERT, UPDATE, ALTER, EXECUTE ON SCHEMA :: [dbo] TO app;


GO
CREATE OR ALTER PROCEDURE sp_getTrainers
AS
BEGIN
		SELECT name, train_id FROM dbo.trainer;
END
GO
EXEC sp_getTrainers;

GO
CREATE OR ALTER PROCEDURE sp_getArenas
AS
BEGIN
	SELECT * FROM vw_arenaLeiter;
END
GO
EXEC sp_getArenas;

GO
CREATE OR ALTER PROCEDURE sp_getPokemonFromTrainer
@trainid int
AS
BEGIN
		SELECT pok_train.fk_train_id AS tid, pok_train.fk_pok_id AS pid, pok_train.lebenspunkte, pokemon.name, pokemon.gewicht, pokemon.groesse, pokemon.max_lebenspunkte,pokemon.typ FROM pokemon
		JOIN pok_train ON pok_train.fk_pok_id = pokemon.pok_id
		JOIN trainer ON trainer.train_id = pok_train.fk_train_id
		WHERE trainer.train_id = @trainid;
END
GO
EXEC sp_getPokemonFromTrainer 12;


GO
CREATE OR ALTER PROCEDURE sp_catchPokemon
@trainer varchar(50), @pokemon varchar(50)
AS
BEGIN
IF((SELECT count(*) FROM trainer WHERE name = @trainer) = 0) OR ((SELECT count(*) FROM pokemon WHERE name = @pokemon) = 0)
	BEGIN
		RAISERROR('Trainer / Pokemon existiert nicht', 16, 1);
	END
ELSE
	DECLARE @trainid int
	DECLARE @pokid int
	DECLARE @health int

	SELECT @trainid = train_id FROM trainer
	WHERE name = @trainer

	SELECT @pokid = pok_id, @health = max_lebenspunkte FROM pokemon
	WHERE name = @pokemon

	INSERT INTO pok_train(fk_pok_id,fk_train_id, lebenspunkte)
	VALUES (@pokid,@trainid,@health)
END
GO

GO
CREATE OR ALTER PROCEDURE sp_learnAttack
@attack varchar(50), @pokemon varchar(50)
AS
BEGIN
IF((SELECT count(*) FROM attacken WHERE name = @attack) = 0) OR ((SELECT count(*) FROM pokemon WHERE name = @pokemon) = 0)
	BEGIN
		RAISERROR('Attacke / Pokemon existiert nicht!', 16, 1);
	END
ELSE
	DECLARE @attid int
	DECLARE @pokid int

	SELECT @attid = att_id FROM attacken
	WHERE name = @attack

	SELECT @pokid = pok_id FROM pokemon
	WHERE name = @pokemon

	INSERT INTO pok_att(fk_pok_id,fk_att_id)
	VALUES (@pokid,@attid)
END

GO
--outputs the total lifepoints of a trainer
CREATE OR ALTER PROCEDURE sp_trainerHealth
@trainid int,
@lifep int output
AS
BEGIN
	SELECT @lifep = SUM(lebenspunkte) FROM pok_train
	WHERE fk_train_id = @trainid
END

--checks which one of the two trainers is a arena head and returns appropriate value
GO
CREATE OR ALTER PROCEDURE sp_checkTrainers
@trainerid1 int, @trainerid2 int
AS
BEGIN
	if(SELECT count(*) FROM arenen WHERE fk_leiter = @trainerid1) = 1 AND (SELECT count(*) FROM arenen WHERE fk_leiter = @trainerid2) = 1
	BEGIN
		RETURN 3
	END
	else if (SELECT count(*) FROM arenen WHERE fk_leiter = @trainerid1) = 1 AND (SELECT count(*) FROM arenen WHERE fk_leiter = @trainerid2) = 0
	BEGIN
		RETURN 1
	END
	else if (SELECT count(*) FROM arenen WHERE fk_leiter = @trainerid1) = 0 AND (SELECT count(*) FROM arenen WHERE fk_leiter = @trainerid2) = 1
	BEGIN
		RETURN 2
	END
	else if (SELECT count(*) FROM arenen WHERE fk_leiter = @trainerid1) = 0 AND (SELECT count(*) FROM arenen WHERE fk_leiter = @trainerid2) = 0
	BEGIN
		RETURN 0
	END
END
