USE Pokemon
GO
DROP VIEW IF EXISTS vw_trainer_pokemon;
GO 

CREATE OR ALTER VIEW vw_trainerPokemon
AS
SELECT trainer.name as trainer, pokemon.name as pokemon 
FROM trainer
INNER JOIN pok_train
ON train_id = fk_train_id
INNER JOIN pokemon
ON pok_id = fk_pok_id
GO

SELECT * FROM vw_trainerPokemon