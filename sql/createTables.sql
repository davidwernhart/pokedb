USE Pokemon
GO
DROP TABLE IF EXISTS arenen;
DROP TABLE IF EXISTS pok_train;
DROP TABLE IF EXISTS trainer;
DROP TABLE IF EXISTS pok_att;
DROP TABLE IF EXISTS pokemon;
DROP TABLE IF EXISTS attacken;
DROP TABLE IF EXISTS typen;
--for index
IF EXISTS (SELECT name FROM sys.indexes WHERE
name = N'IX_pok_id')
DROP INDEX IX_pok_id ON
pok_train; 

--for index
IF EXISTS (SELECT name FROM sys.indexes WHERE
name = N'IX_lebenspunkte')
DROP INDEX IX_lebenspunkte ON
pok_train; 


CREATE TABLE pokemon(
    pok_id int PRIMARY KEY NOT NULL IDENTITY(1,1),
    name varchar(50) NOT NULL,
    typ varchar(10) NOT NULL,
    groesse int NULL,
    gewicht int NULL,
	max_lebenspunkte int NOT NULL

);

CREATE TABLE attacken(
    att_id int PRIMARY KEY NOT NULL IDENTITY(1,1),
    name varchar(50) NOT NULL,
    angriffspunkte int NOT NULL,
    typ varchar(10) NOT NULL
);

--hilfstabelle pokemon-attacken
CREATE TABLE pok_att(
    pok_att_id int PRIMARY KEY NOT NULL IDENTITY(1,1),
    fk_pok_id int NOT NULL,
    fk_att_id int NOT NULL,
    FOREIGN KEY (fk_pok_id) REFERENCES pokemon(pok_id),
    FOREIGN KEY (fk_att_id) REFERENCES attacken(att_id)
);

CREATE TABLE trainer(
    train_id int PRIMARY KEY NOT NULL IDENTITY(1,1),
    name varchar(50) NOT NULL,
    age int NOT NULL,
    orden int NOT NULL,
    region varchar(50) NULL
);

--hilfstabelle pokemon-trainer
CREATE TABLE pok_train(
    pok_train_id int PRIMARY KEY NOT NULL IDENTITY(1,1),
    fk_pok_id int NOT NULL,
    fk_train_id int NOT NULL,
	lebenspunkte int NOT NULL
    FOREIGN KEY (fk_pok_id) REFERENCES pokemon(pok_id), 
    FOREIGN KEY (fk_train_id) REFERENCES trainer(train_id)
);

CREATE TABLE arenen(
    arena_id int PRIMARY KEY NOT NULL IDENTITY(1,1),
    name varchar(50) NOT NULL,
    ort varchar(50) NOT NULL,
    stufe int NOT NULL,
    fk_leiter int NOT NULL,
    FOREIGN KEY (fk_leiter) REFERENCES trainer(train_id)
);

--DROP TABLE typen;

CREATE TABLE typen(
    typ varchar(10) NOT NULL,
    effektiv_gegen varchar(10) NULL,
    schwach_gegen varchar(10) NULL,
)
--index on pok_train.fk_pok_id
GO
CREATE NONCLUSTERED INDEX
IX_pok_id ON pok_train
(fk_pok_id);
GO 
--index on pok_train.lebenspunkte
GO
CREATE NONCLUSTERED INDEX
IX_lebenspunkte ON pok_train
(lebenspunkte);
GO 