USE Pokemon
GO

CREATE OR ALTER VIEW vw_pokemonAttacken
AS
SELECT pokemon.name as pokemon, attacken.name as attacke
FROM pokemon
INNER JOIN pok_att
ON pok_id = fk_pok_id
INNER JOIN attacken
ON att_id = fk_att_id
GO

SELECT * FROM vw_pokemonAttacken