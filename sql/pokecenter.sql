USE Pokemon
GO

CREATE OR ALTER PROCEDURE sp_pokecenter
@trainerid int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	BEGIN TRANSACTION

	IF(SELECT count(*) FROM trainer WHERE train_id = @trainerid) = 0
		BEGIN
			RAISERROR('Trainer existiert nicht', 16, 1);
		END

	ELSE
		BEGIN
			print 'starting to heal pokemon'

			DECLARE @poktrainid int
			DECLARE @maxhealth int
	
			DECLARE cr_healpokemon CURSOR FOR 
			SELECT pok_train_id, pokemon.max_lebenspunkte
			FROM pokemon
			JOIN pok_train 
			ON pok_train.fk_pok_id = pokemon.pok_id
			JOIN trainer 
			ON trainer.train_id = pok_train.fk_train_id
			WHERE trainer.train_id = @trainerid
	
			OPEN cr_healpokemon
				FETCH NEXT FROM cr_healpokemon INTO @poktrainid, @maxhealth
				WHILE @@FETCH_STATUS = 0
				BEGIN
					print @poktrainid
					UPDATE pok_train SET lebenspunkte = @maxhealth
					WHERE pok_train_id = @poktrainid AND fk_train_id = @trainerid

					FETCH NEXT FROM cr_healpokemon INTO @poktrainid, @maxhealth
				END

			CLOSE cr_healpokemon
			DEALLOCATE cr_healpokemon

			print 'healing done'
	END

	COMMIT TRANSACTION
END

/*For testing*/
--UPDATE pok_train SET lebenspunkte = 10
--WHERE fk_train_id = 3

--EXECUTE sp_pokecenter 1;

--EXECUTE sp_pokecenter 6;