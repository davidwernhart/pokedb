
import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class DBHandler {

    private static final String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    private static final String connectionUrl = "jdbc:sqlserver://localhost:1433;databaseName=Pokemon;user=app;password=test";

    private static DBHandler instance = null;
    private Connection con = null;

    public static DBHandler getInstance() {
        if(instance == null)
            instance = new DBHandler();
        return instance;
    }

    public DBHandler(){
        try {
            Class.forName(driver);
            con = DriverManager.getConnection(connectionUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Pokemon> getPokemonFromTrainer(int trainerid){
        System.out.println("Trainer ID: " + trainerid);
        final String query = "EXEC sp_getPokemonFromTrainer ?;";
        try {
            PreparedStatement statement = con.prepareStatement(query);
            statement.setInt(1,trainerid);
            ResultSet set = statement.executeQuery();
            //Map<Integer,String> returnmap = new TreeMap<>();
            List<Pokemon> returnlist = new LinkedList<>();
            while(set.next()){
                //returnmap.put(set.getInt("pok_train_id"),set.getString("name"));
                Pokemon pkmn = new Pokemon(
                        set.getInt("pid"),
                        set.getInt("tid"),
                        set.getInt("lebenspunkte"),
                        set.getString("name"),
                        set.getInt("gewicht"),
                        set.getInt("groesse"),
                        set.getInt("max_lebenspunkte"),
                        set.getString("typ"));

                returnlist.add(pkmn);
            }
            return returnlist;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Map<String,Integer> getTrainers(){
        final String query = "EXEC sp_getTrainers;";
        try {
            PreparedStatement statement = con.prepareStatement(query);
            ResultSet set = statement.executeQuery();
            Map<String,Integer> returnmap = new TreeMap<>();
            while(set.next()){
                returnmap.put(set.getString("name"),set.getInt("train_id"));
            }

            return returnmap;
        }

        catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Map<String,String> getArenas(){
        final String query = "EXEC sp_getArenas;";
        try {
            PreparedStatement statement = con.prepareStatement(query);
            ResultSet set = statement.executeQuery();
            Map<String,String> returnmap = new TreeMap<>();
            while(set.next()){
                returnmap.put(set.getString("arena"),set.getString("trainer"));
            }

            return returnmap;
        }

        catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void pokeCenter(int trainerid){
        final String query = "EXEC sp_pokecenter ?";
        System.out.println("pokecenter: " + trainerid);
        try{
            PreparedStatement statement = con.prepareStatement(query);
            statement.setInt(1,trainerid);
            statement.executeQuery();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void fight(int id1, int id2){
        final String query = "EXEC sp_fight ?,?;";
        try{
            PreparedStatement statement = con.prepareStatement(query);
            statement.setInt(1,id1);
            statement.setInt(2,id2);
            statement.executeQuery();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
