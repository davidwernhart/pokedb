public class Pokemon {

    private String name;
    private int pok_id;
    private int train_id;
    private int lebenspunkte;
    private int max_lebenspunkte;
    private int gewicht;
    private int groesse;
    private String typ;

    public Pokemon(int pok_id, int train_id, int lebenspunkte, String name, int gewicht, int groesse, int max_lebenspunkte, String typ){
        this.pok_id = pok_id;
        this.name = name;
        this.train_id = train_id;
        this.lebenspunkte = lebenspunkte;
        this.max_lebenspunkte = max_lebenspunkte;
        this.gewicht = gewicht;
        this.groesse = groesse;
        this.typ = typ;
    }

    public String getName(){
        return name;
    }

    public int getPok_id() {
        return pok_id;
    }

    public int getTrain_id() {
        return train_id;
    }

    public int getLebenspunkte() {
        return lebenspunkte;
    }

    public int getMax_lebenspunkte() {
        return max_lebenspunkte;
    }

    public int getGewicht() {
        return gewicht;
    }

    public int getGroesse() {
        return groesse;
    }

    public String getTyp() {
        return typ;
    }
}
