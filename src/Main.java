import javax.swing.*;
public class Main {

    public static void main(String[] args) {
        JFrame frame = new JFrame("PokeDB");
        frame.setContentPane(new PokeDB().MainView);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
