import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Map;


public class PokeDB {
    public JPanel MainView;
    private JComboBox comboBox1;
    private JComboBox comboBox2;
    private JButton fight;
    private JButton pokecenter1;
    private JList list1;
    private JList list2;
    private JButton pokecenter2;
    private JLabel arenaLabel;

    private Map<String,Integer> trainers;

    public PokeDB(){
        trainers = DBHandler.getInstance().getTrainers();
        for(Map.Entry<String,Integer> entry : trainers.entrySet()){
            comboBox1.addItem(entry.getKey());
            comboBox2.addItem(entry.getKey());
        }
        comboBox1.addActionListener(e -> {
            if(e.getActionCommand().equals("comboBoxChanged")){
                updateList(list2,comboBox1);
             }
        });

        comboBox2.addActionListener(e -> {
            if(e.getActionCommand().equals("comboBoxChanged")){
                updateList(list1,comboBox2);
            }
        });

        pokecenter2.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 1) {
                    String name = (String) comboBox1.getItemAt(comboBox1.getSelectedIndex());
                    DBHandler.getInstance().pokeCenter(trainers.get(name));
                    updateList(list2,comboBox1);
                    updateList(list1,comboBox2);
                }
            }
        });

        pokecenter1.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 1) {
                    String name = (String) comboBox2.getItemAt(comboBox2.getSelectedIndex());
                    DBHandler.getInstance().pokeCenter(trainers.get(name));
                    updateList(list2,comboBox1);
                    updateList(list1,comboBox2);
                }
            }
        });

        fight.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 1) {
                    String name1 = (String) comboBox1.getItemAt(comboBox1.getSelectedIndex());
                    String name2 = (String) comboBox2.getItemAt(comboBox2.getSelectedIndex());
                    DBHandler.getInstance().fight(trainers.get(name1),trainers.get(name2));
                    updateList(list2,comboBox1);
                    updateList(list1,comboBox2);
                    updateGyms();
                }
            }
        });

        updateList(list1,comboBox2);
        updateList(list2,comboBox1);

        setListListener(list1);
        setListListener(list2);

        updateGyms();
    }

    private void updateList(JList list, JComboBox box){

        list.removeAll();

        String name = (String) box.getItemAt(box.getSelectedIndex());
        java.util.List<Pokemon> pkmns = DBHandler.getInstance().getPokemonFromTrainer(trainers.get(name));

        list.setCellRenderer(new DefaultListCellRenderer() {

            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index,
                                                          boolean isSelected, boolean cellHasFocus) {
                Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                if (value instanceof Pokemon) {
                    Pokemon pkmn = (Pokemon) value;
                    float percentage = 0.3f/((float)pkmn.getMax_lebenspunkte() / (float)pkmn.getLebenspunkte());
                    if(percentage == 0f)
                        setBackground(Color.lightGray);
                    else
                        setBackground(Color.getHSBColor(percentage,1f,1f));
                    setText(pkmn.getName());
                } else {
                    setText("whodat?");
                }
                return c;
            }

        });

        DefaultListModel<Pokemon> model = new DefaultListModel<>();
        for(Pokemon pkmn : pkmns){
            model.addElement(pkmn);
        }
        list.setModel(model);
    }

    private void setListListener(JList list){
        list.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    Pokemon selected = (Pokemon) list.getSelectedValue();
                    System.out.println(selected.getName());

                    JFrame frame = new JFrame(selected.getName());
                    frame.setContentPane(new Details(selected).View);
                    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    frame.pack();
                    frame.setVisible(true);
                }
            }
        });
    }

    private void updateGyms(){
        Map<String,String> arenas = DBHandler.getInstance().getArenas();
        StringBuilder builder = new StringBuilder();
        builder.append("<html>Gym Leaders:<br/>----------------------<br/><br/>");
        for(Map.Entry<String,String> entry : arenas.entrySet()){
            builder.append(entry.getKey() + ":<br/>");
            builder.append(entry.getValue() + "<br/><br/>");
        }
        builder.append("</html>");
        arenaLabel.setText(builder.toString());
    }
}
