import javax.swing.*;
import java.awt.*;

public class Details {
    public JPanel View;
    private JProgressBar healthBar;
    private JLabel healthLabel;
    private JLabel nameLabel;
    private JLabel pkmnIdLabel;
    private JLabel weightLabel;
    private JLabel typeLabel;
    private JLabel trainerIdLabel;
    private JLabel sizeLabel;

    private Pokemon pokemon;

    public Details(Pokemon pokemon){
        this.pokemon = pokemon;

        nameLabel.setText("Name: " + pokemon.getName());
        pkmnIdLabel.setText("Pkmn Id: " + pokemon.getPok_id());
        trainerIdLabel.setText("Trainer Id: " + pokemon.getTrain_id());
        weightLabel.setText("Weight: " + pokemon.getGewicht() + "kg");
        sizeLabel.setText("Size: " + pokemon.getGroesse() + "cm");
        typeLabel.setText("Type: " + pokemon.getTyp());
        healthLabel.setText(pokemon.getLebenspunkte() + " of " + pokemon.getMax_lebenspunkte() + " HP");

        float colorPercentage = 0.3f/((float)pokemon.getMax_lebenspunkte() / (float)pokemon.getLebenspunkte());
        healthBar.setStringPainted(true);
        if(colorPercentage == 0f)
            healthBar.setForeground(Color.lightGray);
        else
            healthBar.setForeground(Color.getHSBColor(colorPercentage,1f,1f));

        float valPercentage = 100f/((float)pokemon.getMax_lebenspunkte() / (float)pokemon.getLebenspunkte());
        healthBar.setValue((int)valPercentage);
    }
}
